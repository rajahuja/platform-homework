from flask import Flask, render_template, request, Response
from flask.json import jsonify
import json
import sqlite3
import time

app = Flask(__name__)

# Setup the SQLite DB
conn = sqlite3.connect('database.db')
conn.execute('CREATE TABLE IF NOT EXISTS readings (device_uuid TEXT, type TEXT, value INTEGER, date_created INTEGER)')
conn.close()

def get_db_con():
    # Set the db that we want and open the connection
    if app.config['TESTING']:
        conn = sqlite3.connect('test_database.db')
    else:
        conn = sqlite3.connect('database.db')
    
    conn.row_factory = sqlite3.Row
    return conn

@app.route('/devices/<string:device_uuid>/readings/', methods = ['POST', 'GET'])
def request_device_readings(device_uuid):
    """
    This endpoint allows clients to POST or GET data specific sensor types.

    POST Parameters:
    * type -> The type of sensor (temperature or humidity)
    * value -> The integer value of the sensor reading
    * date_created -> The epoch date of the sensor reading.
        If none provided, we set to now.

    Optional Query Parameters:
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    * type -> The type of sensor value a client is looking for
    """

    if request.method == 'POST':
        # Grab the post parameters
        post_data = json.loads(request.data)
        sensor_type = post_data.get('type')
        if sensor_type is None:
            return 'Bad request; missing type', 400
        elif sensor_type not in ['temperature', 'humidity']:
            return 'Bad request; invalid type: {}'.format(sensor_type), 400

        value = post_data.get('value')
        if (value < 0 or value > 100):
            return 'Bad request; invalid value: {}'.format(value), 400

        date_created = post_data.get('date_created', int(time.time()))

        conn = get_db_con()
        cur = conn.cursor()
        # Insert data into db
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (device_uuid, sensor_type, value, date_created))
        
        conn.commit()
        conn.close()

        # Return success
        return 'success', 201

    else:

        sensor_type = request.args.get('type', None)
        start = request.args.get('start', None)
        end = request.args.get('end', None)
        
        query = 'select * from readings where device_uuid="{}"'.format(device_uuid) 
        
        if sensor_type is not None:
            if sensor_type not in ['temperature', 'humidity']:
                return 'Bad request; invalid type: {}'.format(sensor_type), 400
            query = query + ' and type="{}"'.format(sensor_type)
        if start is not None:
            query = query + ' and date_created>=' + start
        if end is not None:
            query = query + ' and date_created<=' + end

        conn = get_db_con()
        cur = conn.cursor()
        # Execute the query
        cur.execute(query)
        rows = cur.fetchall()
        conn.close()

        # Return the JSON
        return jsonify([dict(zip(['device_uuid', 'type', 'value', 'date_created'], row)) for row in rows]), 200

@app.route('/devices/<string:device_uuid>/readings/min/', methods = ['GET'])
def request_device_readings_min(device_uuid):
    """
    This endpoint allows clients to GET the min sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    sensor_type = request.args.get('type', None)
    if sensor_type is None:
        return 'Bad request; missing type', 400
    elif sensor_type not in ['temperature', 'humidity']:
        return 'Bad request; invalid type: {}'.format(sensor_type), 400

    start = request.args.get('start', None)
    end = request.args.get('end', None)
    
    query = 'select device_uuid, type, min(value), date_created from readings \
        where device_uuid="{}" and type="{}"'.format(device_uuid,sensor_type)
    if start is not None:
        query = query + ' and date_created>=' + start
    if end is not None:
        query = query + ' and date_created<=' + end
    
    conn = get_db_con()
    cur = conn.cursor()
    # Execute the query
    cur.execute(query)
    row = cur.fetchone()
    conn.close()

    # Return the JSON
    return jsonify(dict(zip(['device_uuid', 'type', 'value', 'date_created'], row))), 200

@app.route('/devices/<string:device_uuid>/readings/max/', methods = ['GET'])
def request_device_readings_max(device_uuid):
    """
    This endpoint allows clients to GET the max sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    sensor_type = request.args.get('type', None)
    if sensor_type is None:
        return 'Bad request; missing type', 400
    elif sensor_type not in ['temperature', 'humidity']:
        return 'Bad request; invalid type: {}'.format(sensor_type), 400
    
    start = request.args.get('start', None)
    end = request.args.get('end', None)
    
    query = 'select device_uuid, type, max(value), date_created from readings \
        where device_uuid="{}" and type="{}"'.format(device_uuid,sensor_type)
    if start is not None:
        query = query + ' and date_created>=' + start
    if end is not None:
        query = query + ' and date_created<=' + end
    
    conn = get_db_con()
    cur = conn.cursor()
    # Execute the query
    cur.execute(query)
    row = cur.fetchone()
    conn.close()

    # Return the JSON
    return jsonify(dict(zip(['device_uuid', 'type', 'value', 'date_created'], row))), 200

@app.route('/devices/<string:device_uuid>/readings/median/', methods = ['GET'])
def request_device_readings_median(device_uuid):
    """
    This endpoint allows clients to GET the median sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    sensor_type = request.args.get('type', None)
    if sensor_type is None:
        return 'Bad request; missing type', 400
    elif sensor_type not in ['temperature', 'humidity']:
        return 'Bad request; invalid type: {}'.format(sensor_type), 400

    start = request.args.get('start', None)
    end = request.args.get('end', None)
    
    query = 'select device_uuid, type, value, date_created from readings \
        where device_uuid="{}" and type="{}"'.format(device_uuid,sensor_type)
    if start is not None:
        query = query + ' and date_created>=' + start
    if end is not None:
        query = query + ' and date_created<=' + end
    query = query + ' order by value'

    conn = get_db_con()
    cur = conn.cursor()
    # Execute the query
    cur.execute(query)
    rows = cur.fetchall()
    conn.close()

    middle = int((len(rows)+1)/2)

    # Return the JSON
    return jsonify(dict(zip(['device_uuid', 'type', 'value', 'date_created'], rows[middle-1]))), 200
    
@app.route('/devices/<string:device_uuid>/readings/mean/', methods = ['GET'])
def request_device_readings_mean(device_uuid):
    """
    This endpoint allows clients to GET the mean sensor readings for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    sensor_type = request.args.get('type', None)
    if sensor_type is None:
        return 'Bad request; missing type', 400
    elif sensor_type not in ['temperature', 'humidity']:
        return 'Bad request; invalid type: {}'.format(sensor_type), 400

    start = request.args.get('start', None)
    end = request.args.get('end', None)
    
    query = 'select avg(value) from readings \
        where device_uuid="{}" and type="{}"'.format(device_uuid,sensor_type)
    if start is not None:
        query = query + ' and date_created>=' + start
    if end is not None:
        query = query + ' and date_created<=' + end
    
    conn = get_db_con()
    cur = conn.cursor()
    # Execute the query
    cur.execute(query)
    row = cur.fetchone()
    conn.close()

    # Return the JSON
    return jsonify(dict(zip(['value'], row))), 200

@app.route('/devices/<string:device_uuid>/readings/mode/', methods = ['GET'])
def request_device_readings_mode(device_uuid):
    """
    This endpoint allows clients to GET the mode sensor reading value for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    sensor_type = request.args.get('type', None)
    if sensor_type is None:
        return 'Bad request; missing type', 400
    elif sensor_type not in ['temperature', 'humidity']:
        return 'Bad request; invalid type: {}'.format(sensor_type), 400

    start = request.args.get('start', None)
    end = request.args.get('end', None)
    
    query = 'select value from readings \
        where device_uuid="{}" and type="{}"'.format(device_uuid,sensor_type)
    if start is not None:
        query = query + ' and date_created>=' + start
    if end is not None:
        query = query + ' and date_created<=' + end
    query = query + ' group by value order by count(*) desc limit 1'

    conn = get_db_con()
    cur = conn.cursor()
    # Execute the query
    cur.execute(query)
    row = cur.fetchone()
    conn.close()

    # Return the JSON
    return jsonify(dict(zip(['value'], row))), 200

@app.route('/devices/<string:device_uuid>/readings/quartiles/', methods = ['GET'])
def request_device_readings_quartiles(device_uuid):
    """
    This endpoint allows clients to GET the 1st and 3rd quartile
    sensor reading value for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    sensor_type = request.args.get('type', None)
    if sensor_type is None:
        return 'Bad request; missing type', 400
    elif sensor_type not in ['temperature', 'humidity']:
        return 'Bad request; invalid type: {}'.format(sensor_type), 400

    start = request.args.get('start', None)
    if start is None:
        return 'Bad request; missing start', 400
 
    end = request.args.get('end', None)
    if end is None:
        return 'Bad request; missing end', 400
    
    query = 'select value from readings \
        where device_uuid="{}" and type="{}"'.format(device_uuid,sensor_type)
    if start is not None:
        query = query + ' and date_created>=' + start
    if end is not None:
        query = query + ' and date_created<=' + end
    query = query + ' order by value'
    
    conn = get_db_con()
    cur = conn.cursor()
    # Execute the query
    cur.execute(query)
    rows = cur.fetchall()
    conn.close()

    q1 = int((len(rows)+1)/4)
    q3 = int((len(rows)+1)*3/4)

    # Return the JSON
    return jsonify({'quartile_1': rows[q1-1][0], 'quartile_3': rows[q3-1][0] }), 200

if __name__ == '__main__':
    app.run()
