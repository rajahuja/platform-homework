# Canary Platform Homework

## Introduction
Imagine a system where hundreds of thousands of Canary like hardware devices are concurrently uploading temperature and humidty sensor data.

The API to facilitate this system accepts creation of sensor records, in addition to retrieval.

These `GET` and `POST` requests can be made at `/devices/<uuid>/readings/`.

Retreival of sensor data should return a list of sensor values such as:

```
    [{
        'date_created': <int>,
        'device_uuid': <uuid>,
        'type': <string>,
        'value': <int>
    }]
```

The API supports optionally querying by sensor type, in addition to a date range.

A client can also access metrics such as the min, max, median, mode and mean over a time range.

These metric requests can be made by a `GET` request to `/devices/<uuid>/readings/<metric>/`

When requesting min, max and median, a single sensor reading dictionary should be returned as seen above.

When requesting the mean or mode, the response should be:

```
    {
        'value': <mean/mode>
    }
```

Finally, the API also supports the retreival of the 1st and 3rd quartile over a specific date range.

This request can be made via a `GET` to `/devices/<uuid>/readings/quartiles/` and should return

```
    {
        'quartile_1': <int>,
        'quartile_3': <int>
    }
```

The API is backed by a SQLite database.

## Getting Started
This service requires Python3. To get started, create a virtual environment using Python3.

Then, install the requirements using `pip install -r requirements.txt`.

Finally, run the API via `python app.py`.

## Testing
Tests can be run via `pytest -v`.

## Tasks
Your task is to fork this repo and complete the following:

- [x] Add field validation. Only *temperature* and *humidity* sensors are allowed with values between *0* and *100*.
- [x] Add logic for query parameters for *type* and *start/end* dates.
- [x] Implement the min endpoint.
- [x] Implement the max endpoint.
- [x] Implement the median endpoint.
- [x] Implement the mean endpoint.
- [x] Implement the mode endpoint.
- [x] Implement the quartile endpoint.
- [x] Add and implement the stubbed out unit tests for your changes.
- [x] Update the README with any design decisions you made and why.


## Design Decisions

### DB Connections ###
- Moved the DB connection call to happen *after* validation, so that connection is not made if request is bad (e.g. missing or invalid type, or value out of range)
- Moved the repeated DB connection logic to a separate function (DRY).
- Added call to close DB connection in each function after done with query. This also resolved the connection errors seen in pytests initially.

**Note**: Currently, each route handler function is making a DB connection. It might be better if we can somehow connect to the DB once that the handlers can share for all incoming API calls. That is, connect once, execute (and commit when appopriate) in each function, disconnect once.

### Statistics Calculations ###
- Wherever possible (e.g. min, max, mean), tried to do computation within SQL query. Other places, e.g. median, got rows from SQL query and then determined the median row in Python code.
- For median, we use the formula: median = int((M+1)/2)<sup>th</sup> row for M rows ordered by the reading value. 
- Hence, if there are an even number of M = 2N rows, with two middle elements, N and N+1, we return the middle element N, instead of average of Nth and (N+1)th, since we need to return sensor date created etc. as well, which could be different for elemenst N and N+1
- Likewise, for simplicity, for 1st and 3rd quartiles, we assume q1 = ((M+1)/4)<sup>th</sup> and q3 = ((M+1)*3/4)<sup>th</sup> row
- For mode, if there's a tie for the most-commonly occuring values, the lowest one is used

### Development Environment(s) Used ###
- For development and basic testing: VS Code 1.40.2 on Windows 10 Pro v1909 with Python 3.8.0
- pytest had some errors running in this environment, so ran pytest using: Python 3.6.8 on Ubuntu Bionic on WSL

### TODO/Possible Future Enhancements ###
- To prevent SQL Injection, replace assembling SQL queries in Python strings with DB-API parameter substitution
- Gracefully handle cases where no matching data found in DB
- Unit-test assertions are currently hard-coded and closely coupled with the values inserted into DB in test setup. This should probably be made decoupled, e.g. by putting the data to be inserted and expected output in a separate file that the setup and test functions can read.
- Add more test cases to handle different error codes, invalid types, invalid dates, missing UUIDs etc.

When you're finished, send your git repo link to Michael Klein at michael@canary.is. If you have any questions, please do not hesitate to reach out!
